<?php namespace App\Repositories;

use App\Repositories\Contracts\AdvertRepositoryInterface;

use App\Advert;

class AdvertRepository implements AdvertRepositoryInterface
{

        public function __construct(Advert $advert)
        {
                $this->advert = $advert;
        }

        public function retrive($id)
        {
            return $this->advert->where('id', $id)->first();
        }

        public function all()
        {
            return $this->advert->paginate(5);
        }

        public function create($request)
        {
          $task = new Advert;
          $task->status = $request->status;
          $task->company_id = $request->company;
          $task->available_from = $request->adverted_from;
          $task->available_to = $request->adverted_to;
          $task->position = $request->position;

          $task->save();
          return $task;
        }

        public function update($request, $id)
        {
            return $this->advert->where('id', $id)->update(array(
              'company_id' => $request->company,
              'available_from' => $request->adverted_from,
              'available_to' => $request->adverted_to,
              'position' => $request->position,
              'status' => $request->status,
            ));
        }

        public function delete($id)
        {
            return $this->advert->where('id', $id)->delete();
        }

}
