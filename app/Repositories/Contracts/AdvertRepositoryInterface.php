<?php namespace App\Repositories\Contracts;

interface AdvertRepositoryInterface
{
    public function all();

    public function create($data);

    public function retrive($id);

    public function update($data, $id);

    public function delete($id);
/*
    public function show($id);
    */
}
