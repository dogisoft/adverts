<?php namespace App\Repositories;

use App\Repositories\Contracts\CompanyRepositoryInterface;

use App\Company;

class CompanyRepository implements CompanyRepositoryInterface
{
        protected $post;

        public function __construct(Company $company)
        {
                $this->company = $company;
        }

        public function retrive($id)
        {
            return $this->company->where('company_id', $id)->first();
        }

        public function all()
        {
            return $this->company->paginate(5);
        }

        public function create($request)
        {
          $task = new Company;
          $task->company_name = $request->companyName;
          $task->owner_name = $request->ownerName;
          $task->email = $request->emailAddress;
          $task->web = $request->website;
          $task->profile = $request->category;
          $task->about = $request->about;
          $task->phone = 'aaa';
          $task->picture = 'picture';
          $task->opening_time = 'opening_time';
          $task->position = 10;
          $task->location = 'location';
          $task->status = 0;
          $task->save();
          return $task;
        }

        public function update($request, $id)
        {
            return $this->company->where('company_id', $id)->update(array(
              'company_name' => $request->companyName,
              'owner_name' => $request->ownerName,
              'email' => $request->emailAddress,
              'web' => $request->website,
              'profile' => $request->category,
              'about' => $request->about,
              'phone' => $request->phone,
              //'picture' => $request->picture,
              'opening_time' => $request->opening_time,
              //'position' => $request->position,
              'location' => $request->location,
              'status' => $request->status,
            ));
        }

        public function delete($id)
        {
            return $this->company->where('company_id', $id)->delete();
        }

}
