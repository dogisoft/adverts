<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCompany();
        $this->registerAdvert();
    }

    public function registerCompany()
    {
        $this->app->bind('App\Repositories\Contracts\CompanyRepositoryInterface', 'App\Repositories\CompanyRepository');
    }

    public function registerAdvert()
    {
        $this->app->bind('App\Repositories\Contracts\AdvertRepositoryInterface', 'App\Repositories\AdvertRepository');
    }

}
