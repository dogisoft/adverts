<?php

namespace App\Helpers;
use App\Company;
use App\Advert;
use Auth;

class AdvertHelper
{
  public static function getListOfCompanies()
  {
    $companies = Company::where('status', 1)->orderBy('company_name', 'ASC')->get();
    return $companies;
  }

  public static function getComp($id)
  {
    $company = Company::where('status', 1)->where('company_id', $id)->first();
    return $company;
  }

  public static function isActive($id)
  {
    $company = Advert::where('status', 1)
    ->where('company_id', $id)
    ->whereDate('available_to', '>=', date('Y-m-d', time()))
    ->whereDate('available_from', '<=', date('Y-m-d', time()))
    ->value('status');

    if($company==1){
      return true;
    }
  }


  public static function getByCategory($slug)
  {
    $profile = 0;

    if($slug == "restaurant")
    {
      $profile = 1;
    }

    if($slug == "hotel")
    {
      $profile = 2;
    }

    if($slug == "bar")
    {
      $profile = 3;
    }

    if($slug == "club")
    {
      $profile = 4;
    }

    if($slug == "bakery")
    {
      $profile = 5;
    }

    if($slug == "green-grocers")
    {
      $profile = 6;
    }

    if($slug == "bucher")
    {
      $profile = 7;
    }

    if($slug == "it-and-telecommunication")
    {
      $profile = 8;
    }

    $company = Company::where('profile', $profile)->where('status', 1)->get();
    return $company;
  }

}
