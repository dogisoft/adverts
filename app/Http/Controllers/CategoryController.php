<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $companies = \AdvertHelper::getByCategory($slug);
        return view('frontend.companies', ['companies' => $companies]);
    }


}
