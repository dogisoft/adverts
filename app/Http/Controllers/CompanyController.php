<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

use App\Repositories\Contracts\CompanyRepositoryInterface;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $company;

    public function __construct(CompanyRepositoryInterface $company)
    {
        $this->middleware('auth');
        $this->company = $company;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->company->all();
        return view('company.manage', ['companies' => $companies]);
    }

    public function createit(Request $request)
    {
        return view('company.create');
    }

    public function newcompany(Request $request)
    {
        $validatedData = $request->validate([
            'companyName' => 'required|max:100',
            'ownerName' => 'required|max:100',
            'emailAddress' => 'required|email|max:100',
            'website' => 'required|max:100',
            'category' => 'required|max:100',
            'about' => 'required',
        ]);

        $success = $this->company->create($request);

        if($success){
            //return view('company.create')->with('status','Company has been created .');
            return back()->with('status', 'Company created successfully.');
        }
        else{
          //return view('company.create');
        }
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'companyName' => 'required|max:100',
            'ownerName' => 'required|max:100',
            'emailAddress' => 'required|email|max:100',
            'website' => 'required|max:100',
            'category' => 'required|max:100',
            'about' => 'required',
        ]);

        $success = $this->company->update($request, $id);

        if($success){
            //return view('company.create')->with('status','Company has been created .');
            return back()->with('status', 'Company updated successfully.');
        }
        else{
          //return view('company.create');
        }
    }

    public function edit($id)
    {
        $company = $this->company->retrive($id);
        return view('company.edit', ['company' => $company]);
    }

    public function delete($id)
    {
        $this->company->delete($id);
        $companies = $this->company->all();
        return view('company.manage', ['companies' => $companies]);
    }

}
