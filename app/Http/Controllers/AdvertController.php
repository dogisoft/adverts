<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
use App\Repositories\Contracts\AdvertRepositoryInterface;

class AdvertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdvertRepositoryInterface $advert)
    {
        $this->middleware('auth');
        $this->advert = $advert;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = $this->advert->all();
        return view('advert.manage', ['adverts' => $adverts]);
    }

    public function createit(Request $request)
    {
        return view('advert.create');
    }

    public function newadvert(Request $request)
    {
      $validatedData = $request->validate([
          'company' => 'required|max:100',
          'adverted_from' => 'required|max:100',
          'adverted_to' => 'required|max:100',
          'position' => 'required|max:100',
          'status' => 'required|max:100',
      ]);

      $success = $this->advert->create($request);

      if($success){
          return back()->with('status', 'Advert created successfully.');
      }
      else{
        return view('advert.create');
      }

    }

    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
          'company' => 'required|max:100',
          'adverted_from' => 'required|max:100',
          'adverted_to' => 'required|max:100',
          'position' => 'required|max:100',
          'status' => 'required|max:100',
      ]);

        $success = $this->advert->update($request, $id);

        if($success){
            //return view('company.create')->with('status','Company has been created .');
            return back()->with('status', 'Advert updated successfully.');
        }
        else{
          //return view('company.create');
        }
    }

    public function edit($id)
    {
        $advert = $this->advert->retrive($id);
        return view('advert.edit', ['advert' => $advert]);
    }

    public function delete($id)
    {
        $this->advert->delete($id);
        $adverts = $this->advert->all();
        return view('advert.manage', ['adverts' => $adverts]);
    }

}
