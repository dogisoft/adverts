<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'company';
    protected $fillable = ['company_name', 'owner_name', 'phone', 'web', 'email', 'profile', 'location', 'picture', 'opening_time', 'about', 'position', 'status'];

}
