@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
          <div class="card">
              <div class="card-header">Edit advert</div>

              <div class="card-body">
                @if (Session::has('status'))
                    <div class="alert alert-success">
                        {{ Session::get('status') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                  <form method="post" action="/advert/update/{{$advert->id}}">
                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="exampleInputEmail1">Company</label>
                      <select class="form-control" name="company" aria-describedby="company" placeholder="Select company">
                        <option value="0">Choose a company</option>
                        @foreach(AdvertHelper::getListOfCompanies() as $company)

                          <option value="{{$company->company_id}}" @if($advert->company_id==$company->company_id) selected @endif>{{$company->company_name}}</option>
                        @endforeach
                      </select>
                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="from">Advertised from</label>
                          <input type="date" name="adverted_from" class="form-control" value="{{$advert->available_from}}" />
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="from">Advertised to</label>
                          <input type="date" name="adverted_to" class="form-control" value="{{$advert->available_to}}" />
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="from">Position</label>
                          <select class="form-control" id="pos" name="position">
                            <option value="0">Choose a position</option>
                            <option value="1" @if($advert->position==1) selected @endif>Left position</option>
                            <option value="2" @if($advert->position==2) selected @endif>Middle position</option>
                            <option value="3" @if($advert->position==3) selected @endif>Right position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="from">Status</label>
                          <select class="form-control" id="status" name="status">
                            <option value="0" @if($advert->status==0) selected @endif>Inactive</option>
                            <option value="1" @if($advert->status==1) selected @endif>Active</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
              </div>
          </div>
        </div>

        <div class="col-md-4">
          @include('components/left_menu')
        </div>
    </div>
</div>
@endsection
