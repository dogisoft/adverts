@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Edit company</div>

        <div class="card-body">
          @if (Session::has('status'))
          <div class="alert alert-success">
            {{ Session::get('status') }}
          </div>
          @endif

          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <form method="post" action="/company/update/{{$company->company_id}}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputCompany1">Company name</label>
                  <input value="{{$company->company_name}}" type="text" class="form-control" value="" name="companyName" id="exampleInputCompany1" aria-describedby="nameHelp" placeholder="Enter company name">
                  <small id="nameHelp" class="form-text text-muted">We'll never share your company name with anyone else.</small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputCompanyName">Company owner's name</label>
                  <input value="{{$company->owner_name}}" type="text" class="form-control" name="ownerName" id="exampleInputCompanyName" aria-describedby="ownerHelp" placeholder="Enter company owner's name">
                  <small id="ownerHelp" class="form-text text-muted">We'll never share your company owner's name with anyone else.</small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input value="{{$company->email}}" autocomplete="off" type="email" name="emailAddress" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputCompanyWeb">Website</label>
                  <input value="{{$company->web}}" type="text" name="website" class="form-control" id="exampleInputCompanyWeb" placeholder="Enter company website">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleTextarea">About the company</label>
                  <textarea name="about" class="form-control" id="about_company" rows="3">{{$company->about}}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Opening time</label>
                  <textarea type="text" name="opening_time" class="form-control" id="opening_time" placeholder="Enter opening_time">{{$company->opening_time}}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleSelect2">Choose category</label>
                  <select name="category" class="form-control" id="exampleSelect2">
                    <option value="1" @if($company->profile==1) selected="selected" @endif>Resturant</option>
                    <option value="2" @if($company->profile==2) selected="selected" @endif>Hotel</option>
                    <option value="3" @if($company->profile==3) selected="selected" @endif>Bar</option>
                    <option value="4" @if($company->profile==4) selected="selected" @endif>Club</option>
                    <option value="5" @if($company->profile==5) selected="selected" @endif>Bakery</option>
                    <option value="6" @if($company->profile==6) selected="selected" @endif>Green grocers</option>
                    <option value="7" @if($company->profile==7) selected="selected" @endif>Bucher</option>
                    <option value="8" @if($company->profile==8) selected="selected" @endif>IT and Telecomunication</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Phone number</label>
                  <input value="{{$company->phone}}" type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number">
                </div>
              </div>

              <!--<div class="col-md-6">
                <div class="form-group">
                  <label for="picture">Picture</label>
                  {{$company->picture}}
                  <input type="file" name="picture" class="form-control" id="file">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Position</label>
                  <input value="{{$company->position}}" type="text" name="position" class="form-control" id="position" placeholder="Enter position">
                </div>
              </div>-->

              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Location</label>
                  <input value="{{$company->location}}" type="text" name="location" class="form-control" id="Location" placeholder="Enter location">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Status</label>
                  <select value="{{$company->status}}" type="text" name="status" class="form-control" id="status" placeholder="Select status">
                    <option value="1" @if($company->status==1) selected="selected" @endif>Active</option>
                    <option value="0" @if($company->status==0) selected="selected" @endif>Inactive</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </div><!-- end of row -->
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      @include('components/left_menu')
    </div>
  </div>
</div>
@endsection
