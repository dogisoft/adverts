@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


<div class="container">
    <div class="row">
        <div class="col-md-8">
          <div class="card">
              <div class="card-header">Manage companies</div>

              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

                  <div class="table-responsive table-striped table-bordered">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Company name</th>
                          <th>Company email</th>
                          <th>State</th>
                          <th class="text-center">EDIT</th>
                          <th class="text-center">DELETE</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($companies as $company)
                        <tr>
                          <td>{{$company->company_name}}</td>
                          <td>{{$company->email}}</td>
                          <td>@if($company->status === 1) Active @else Inactive @endif</td>

                          <td class="text-center"><a href="/company/edit/{{$company->company_id}}"><i class="far fa-edit"></i></a></td>
                          <td class="text-center"><a href="/company/delete/{{$company->company_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                    </table>
                    <div><?php echo $companies->render(); ?></div>
                  </div>

              </div>
          </div>
        </div>

        <div class="col-md-4">
          @include('components/left_menu')
        </div>
    </div>
</div>
@endsection
