@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
          <div class="card">
              <div class="card-header">Create new company</div>

              <div class="card-body">
                  @if (Session::has('status'))
                      <div class="alert alert-success">
                          {{ Session::get('status') }}
                      </div>
                  @endif

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  <form method="post" action="/company/create_new">
                     {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputCompany1">Company name</label>
                      <input type="text" class="form-control" value="" name="companyName" id="exampleInputCompany1" aria-describedby="nameHelp" placeholder="Enter company name">
                      <small id="nameHelp" class="form-text text-muted">We'll never share your company name with anyone else.</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputCompanyName">Company owner's name</label>
                      <input type="text" class="form-control" name="ownerName" id="exampleInputCompanyName" aria-describedby="ownerHelp" placeholder="Enter company owner's name">
                      <small id="ownerHelp" class="form-text text-muted">We'll never share your company owner's name with anyone else.</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input autocomplete="off" type="email" name="emailAddress" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputCompanyWeb">Website</label>
                      <input type="text" name="website" class="form-control" id="exampleInputCompanyWeb" placeholder="Enter company website">
                    </div>

                    <div class="form-group">
                      <label for="exampleSelect2">Choose category</label>
                      <select name="category" class="form-control" id="exampleSelect2">
                        <option value="1">Resturant</option>
                        <option value="2">Hotel</option>
                        <option value="3">Bar</option>
                        <option value="4">Club</option>
                        <option value="5">Bakery</option>
                        <option value="6">Green grocers</option>
                        <option value="7">Bucher</option>
                        <option value="8">IT and Telecomunication</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleTextarea">About the company</label>
                      <textarea name="about" class="form-control" id="about_company" rows="3"></textarea>
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
              </div>
          </div>
        </div>

        <div class="col-md-4">
          @include('components/left_menu')
        </div>
    </div>
</div>
@endsection
