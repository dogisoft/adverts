<div class="card">
    <div class="card-header">Dashboard</div>

    <div class="card-body">
      @auth
          <div class="menu-item"> <a href="{{ url('/home') }}"> Home</a></div>

          <br><b><i class="far fa-address-book"></i> Companies</b><hr>
          <div class="menu-item"><a href="{{ url('/company/create') }}"> Create new company</a></div>
          <div class="menu-item"><a href="{{ url('/manage/companies') }}">Manage companies</a></div>

          <br><b><i class="fab fa-adversal"></i> Adverts</b><hr>
          <div class="menu-item"><a href="{{ url('/advert/create_new') }}"> Create new advert</a></div>
          <div class="menu-item"><a href="{{ url('/manage/adverts') }}">Manage adverts</a></div>

      @else
      <ul>
        <li><a href="/category/restaurant">Restaurants</a></li>
        <li><a href="/category/bar">Bars</a></li>
        <li><a href="/category/hotel">Hotels</a></li>
        <li><a href="/category/club">Clubs</a></li>
        <li><a href="/category/bakery">Bakeries</a></li>
        <li><a href="/category/green-grocers">Green grocers</a></li>
        <li><a href="/category/bucher">Buchers</a></li>
        <li><a href="/category/it-and-telecommunication">IT and Telecomunications</a></li>
      </ul>
      @endauth
    </div>
</div>
