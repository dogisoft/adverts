@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8">

      @if (Session::has('status'))
      <div class="alert alert-success">
        {{ Session::get('status') }}
      </div>
      @endif

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="row">

      @foreach($companies as $company)
      <div class="col-md-6">
        <div class="cardItem">
          <div class="card-header">{{$company->company_name}}</div>
          <div class="card-body">
            <div class="panel">
              @if(AdvertHelper::isActive($company->company_id)===true && $company->status == 1)
              <div class="label"><b>CEO:</b> {{$company->owner_name}}</div>
              <div class="label"><b>Address:</b> {{$company->location}}</div>
              <div class="label"><b>Phone:</b> {{$company->phone}}</div>
              <div class="label"><b>About us:</b> {{$company->about}}</div>
              @endif
            </div>
          </div>
          <div class="card-footer"><a href="http://{{$company->web}}">{{$company->web}}</a> | <a href="mailto:{{$company->email}}">{{$company->email}}</a></div>
        </div>
        </div>
      @endforeach

      @if(!isset($company))
        <div class="no-entry">There is no entry in {{Request::segment(2)}} category.</div>
      @endif
     </div>

    </div>

    <div class="col-md-4">
      @include('components/left_menu')
    </div>
  </div>
</div>
@endsection
