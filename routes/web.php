<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('/manage/companies', 'CompanyController@index');
Route::get('/company/create', 'CompanyController@createit');

Route::post('/company/create_new', 'CompanyController@newcompany');
Route::get('/company/delete/{item}', 'CompanyController@delete');
Route::get('/company/edit/{item}', 'CompanyController@edit');
Route::post('/company/update/{item}', 'CompanyController@update');


Route::get('/advert/create_new', 'AdvertController@createit');
Route::get('/manage/adverts', 'AdvertController@index');
Route::post('/advert/create_new', 'AdvertController@newadvert');
Route::get('/advert/delete/{item}', 'AdvertController@delete');
Route::get('/advert/edit/{item}', 'AdvertController@edit');
Route::post('/advert/update/{item}', 'AdvertController@update');
Route::get('/category/{item}', 'CategoryController@index');
